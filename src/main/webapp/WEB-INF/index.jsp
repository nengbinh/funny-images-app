<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Funny Images app</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="index.js"></script>
    <link rel="stylesheet" href="index.css">
</head>
<body>

<div class="panel panel-default">

    <div class="panel-body search" style="padding-top:8px;padding-bottom:8px;border-top-left-radius: 5px;border-top-right-radius:5px;background-color:cadetblue">
        <div class="input-group">
            <span class="input-group-addon" style="background-color:cadetblue;color:white;border:0px"><b>Searching Keyword</b></span>
            <input type="text" id='search_text' onkeypress="enterKey(event)" oninput="autocompleted()" class="form-control" placeholder=".........." aria-describedby="basic-addon1">
        </div>
        <button class="btn btn-default" id="search_button">Go!</button>
    </div>

    <div class="panel-body tags">
        <ul class="pager" id="suggest_list">
        </ul>
    </div>

    <div class="panel-body imgs" id="img_container">
        <div class="masonry" id="result_box"></div>
    </div>

</div>

</body>
</html>
