
window.onload = function() {
    // button click
    $("#search_button").on("click", function() {
        searchh($("#search_text").val());
    });

    // scroll bottom
    $("#img_container").scroll(function() {
        var h = $(this).height();
        var sh = $(this)[0].scrollHeight;
        var st =$(this)[0].scrollTop;
        if(h+st>=sh){
            show();
        }
    });

}

    var Gdata;

    // auto complete
    function autocompleted() {
        $.ajax({
            type: "GET",
            url: "/auto/" + $("#search_text").val(),
            dataType: "json",
            success: function(data){
                showWord(data, false);
            }
        });
    }

    // get suggest
    function suggest(keyword) {
        $.ajax({
            type: "GET",
            url: "/suggest/" + keyword,
            dataType: "json",
            success: function(data){
                showWord(data);
                $("#search_text").val(keyword);
            }
        });
    }

    // search for images
    function searchh(keyword) {
        $.ajax({
            type: "GET",
            url: "/search/" + keyword,
            dataType: "json",
            success: function(data){
                $("#result_box").html("");
                suggest(keyword);
                Gdata = data.results;
                show();
            }
        });
    }

    // enter
    function enterKey(event) {
        if(event.keyCode==13){
            searchh($("#search_text").val());
        }
    }


    /////////   show process //////////

    // put it in box
    function show() {
        var height = 210;
        count = 0;
        while(Gdata.length && count<10) {
            let size = Gdata[0]['media'][0]['tinygif']['dims'][0] / height;
            let h = Gdata[0]['media'][0]['tinygif']['dims'][1] * size;
            $("#result_box").append("<div class='item' style=\"background-image: url('"+Gdata[0]['media'][0]['tinygif']['url']+"');height:"+h+"px\"></div>");
            Gdata.splice(0,1);
            count ++;
        }
    }

    // suggest word
    function showWord(datas, type=true) {
        let text = "";
        for(let data of datas['results']) {
            if(type) {
                text += "<li><a onClick=\"return searchh('"+data+"')\" style='background-color:darkorange;color:white;'>"+data+"</a></li>";
            }else{
                text += "<li><a onclick=\"return searchh('"+data+"')\">"+data+"</a></li>";
            }
        }
        $("#suggest_list").html(text);
    }