package com.funnyimage.pictures;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class PageController {

    @RequestMapping("/")
    public String indexPage() {
        return "index.jsp";
    }

}
