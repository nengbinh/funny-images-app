package com.funnyimage.pictures;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

import static org.apache.http.HttpHeaders.USER_AGENT;

@RestController
public class API {

    private final String KEY = "NN6W24AH8IAQ";

    private JSONObject getAPIdata(String url) {
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        // add request header
        request.addHeader("User-Agent", USER_AGENT);
        HttpResponse response;
        JSONObject jsonObj = null;
        try{
            response = client.execute(request);
            HttpEntity httpEntity = response.getEntity();
            String apiOutput = EntityUtils.toString(httpEntity);
            jsonObj = new JSONObject(apiOutput);
        }catch(IOException e) { }
        //
        return jsonObj;
    }

    @RequestMapping("/search/{keyword}")
    public Object search(@PathVariable("keyword") String keyword) {

        String url = "https://api.tenor.com/v1/search?key="+KEY+"&q="+keyword+"&limit=50";
        url = url.replaceAll(" ","%20");
        JSONObject data = this.getAPIdata(url);
        return data.toMap();
    }

    @RequestMapping("/suggest/{keyword}")
    public Object suggest(@PathVariable("keyword") String keyword) {
        String url = "https://api.tenor.com/v1/search_suggestions?key="+KEY+"&q="+keyword+"&limit=4";
        url = url.replaceAll(" ","%20");
        JSONObject data = this.getAPIdata(url);
        return data.toMap();
    }

    @RequestMapping("/auto/{keyword}")
    public Object autoCompleted(@PathVariable("keyword") String keyword) {
        String url = "https://api.tenor.com/v1/autocomplete?key="+KEY+"&q="+keyword;
        url = url.replaceAll(" ","%20");
        JSONObject data = this.getAPIdata(url);
        return data.toMap();
    }
}